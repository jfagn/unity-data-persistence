using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

[DefaultExecutionOrder(1000)]
public class StartMenuHandler : MonoBehaviour
{
    public TMP_InputField username;

    // Start is called before the first frame update
    public void StartGame()
    {
        UpdateName(username.text);
        SceneManager.LoadScene(1);
    }

    private void UpdateName(string name)
    {
        NameManager.Instance.name = name;
    }
}
